import Vue from 'vue'
import App from './App.vue'

// Import Bootstrap and BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import { BootstrapVue } from 'bootstrap-vue'
import { ModalPlugin } from 'bootstrap-vue'
// This imports the dropdown and table plugins
import { DropdownPlugin, TablePlugin } from 'bootstrap-vue'
import { BModal, VBModal } from 'bootstrap-vue'
import { BTable } from 'bootstrap-vue'
import router from './router'
import "./css/main.css";

Vue.config.productionTip = false
Vue.component('BModal', BModal)
// Note that Vue automatically prefixes directive names with `v-`
Vue.directive('b-modal', VBModal)
Vue.directive('b-table', BTable)
Vue.use(ModalPlugin)
Vue.use(DropdownPlugin)
Vue.use(TablePlugin)
Vue.use(BootstrapVue)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
