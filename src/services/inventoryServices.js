import http from "../../http-common.js";

class inventoryServices {
  getAll() {
    return http.get("/Inventory");
  }

  get(id) {
    return http.get(`/Inventory/${id}`);
  }
.000
  create(data) {
    return http.post("/Inventory", data);
  }

  update(id, data) {
    return http.put(`/Inventory/${id}`, data);
  }

  delete(id) {
    return http.delete(`/Inventory/${id}`);
  }

  deleteAll() {
    return http.delete(`/Inventory`);
  }

  findByTitle(title) {
    return http.get(`/Inventory?title=${title}`);
  }

  //Category
  
  getAllCategory() {
    return http.get("/category");
  }

  getCategory(id) {
    return http.get(`/category/${id}`);
  }
.000
  createCategory(data) {
    return http.post("/category", data);
  }

  updateCategory(id, data) {
    return http.put(`/category/${id}`, data);
  }

  deleteCategory(id) {
    return http.delete(`/category/${id}`);
  }
}

export default new inventoryServices();
