import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: "/",
    alias: "/inventory",
    name: "inventory",
    component: () => import("../components/Inventory")
  },
  {
    path: "/inventory/:id",
    name: "inventory-details",
    component: () => import("../components/InventoryEdit")
  },
  {
    path: "/add",
    name: "add",
    component: () => import("../components/AddInventory")
  },
  {
    path: "/edit/:id",
    name: "edit",
    component: () => import("../components/InventoryEdit")
  },
  //Category
  {
    path: "/category",
    name: "category-list",
    component: () => import("../components/Category-List")
  }
  // ,  {
  //   path: "/categoryedit/:id",
  //   name: "categoryedit",
  //   component: () => import("../components/CategoryEdit")
  // }
]

const router = new VueRouter({
  routes
})

export default router
