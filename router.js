import { createWebHistory, createRouter } from "vue-router";

const routes =  [
  {
    path: "/",
    alias: "/inventory",
    name: "inventory",
    component: () => import("./components/InventoryList")
  },
  {
    path: "/inventory/:id",
    name: "inventory-details",
    component: () => import("./components/Inventory")
  },
  {
    path: "/add",
    name: "add",
    component: () => import("./components/AddInventory")
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;